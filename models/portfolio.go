package models

import (
	"database/sql"
	"fmt"
)

type PortfolioModel struct {
}

type CreateNewPortfolioParams struct {
	Name     string `json:"name" binding:"required"`
	Currency int    `json:"currency" binding:"required"`
}

func (u PortfolioModel) CreateNewPortfolio(db *sql.DB, params CreateNewPortfolioParams) int {
	logHead := "[Models][CreateNewPortfolio]"
	fmt.Printf("%s STARTS - params: %#v", logHead, params)

	id := -1
	insertionError := db.QueryRow(
		fmt.Sprintf("INSERT INTO portfolio (name, currency) VALUES ($1, $2) RETURNING id;"),
		params.Name,
		params.Currency,
	).Scan(&id)

	if insertionError != nil {
		fmt.Printf(
			"%s FAILED - Cannot insert new portfolio with name: '%s' and currency: '%d' in database. %s\n",
			logHead,
			params.Name,
			params.Currency,
			insertionError.Error(),
		)
		return id
	}

	fmt.Printf("%s DONE - portfolioId: %d", logHead, id)
	return id
}
