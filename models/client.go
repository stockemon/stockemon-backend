package models

import (
	"database/sql"
	"fmt"
	"stockemon-backend/utils"
)

type ClientModel struct {
}

type Client struct {
	Id          int    `json:"id"`
	Username    string `json:"username"`
	DisplayName string `json:"display_name"`
	Password    string `json:"password"`
}

type CreateNewClientParams struct {
	Username    string `json:"username" binding:"required"`
	DisplayName string `json:"display_name"`
	Password    string `json:"password" binding:"required"`
}

func (u ClientModel) GetClientByName(db *sql.DB, username string) (bool, Client) {
	logHead := "[Models][GetClientByName]"
	fmt.Printf("%s STARTS - username: '%s'\n", logHead, username)

	var client Client
	query := fmt.Sprintf("SELECT id, username, display_name, password FROM client WHERE username = $1;")
	queryError := db.QueryRow(query, username).Scan(&client.Id, &client.Username, &client.DisplayName, &client.Password)

	if queryError != nil {
		fmt.Printf(
			"%s FAILED - Cannot find client with username: '%s' in database. %s\n",
			logHead,
			username,
			queryError.Error(),
		)
		return false, Client{}
	}

	fmt.Printf("%s DONE - %#v\n", logHead, client)
	return true, client
}

func (u ClientModel) CreateNewClient(db *sql.DB, params CreateNewClientParams) int {
	logHead := "[Models][CreateNewClient]"
	fmt.Printf("%s STARTS - params: %#v", logHead, params)

	id := -1
	insertionError := db.QueryRow(
		fmt.Sprintf("INSERT INTO client (username, display_name, password) VALUES ($1, $2, $3) RETURNING id;"),
		params.Username,
		params.DisplayName,
		utils.GeneratePasswordHash([]byte(params.Password)),
	).Scan(&id)

	if insertionError != nil {
		fmt.Printf(
			"%s FAILED - Cannot insert new client with username: '%s' in database. %s\n",
			logHead,
			params.Username,
			insertionError.Error(),
		)
		return id
	}

	fmt.Printf("%s DONE - clientId: %d", logHead, id)
	return id
}
