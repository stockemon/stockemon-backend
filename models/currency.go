package models

import (
	"database/sql"
	"fmt"
)

type CurrencyModel struct {
}

type Currency struct {
	Id     int    `json:"id"`
	Ticker string `json:"ticker"`
}

type CreateNewCurrencyParams struct {
	Ticker string `json:"ticker" binding:"required"`
}

func (u CurrencyModel) GetAllCurrencies(db *sql.DB) (bool, []Currency) {
	logHead := "[Models][GetAllCurrencies]"
	fmt.Printf("%s STARTS", logHead)

	currencies := []Currency{}
	rows, queryError := db.Query("SELECT id, ticker FROM currency;")
	defer rows.Close()

	if queryError != nil {
		fmt.Printf(
			"%s FAILED - Cannot query all currencies available from database. %s\n",
			logHead,
			queryError.Error(),
		)
		return false, currencies
	}

	for rows.Next() {
		var currencyRecord Currency
		rows.Scan(&currencyRecord.Id, &currencyRecord.Ticker)
		currencies = append(currencies, currencyRecord)
	}

	fmt.Printf("%s DONE - %#v\n", logHead, currencies)
	return true, currencies
}

func (u ClientModel) CreateNewCurrency(db *sql.DB, params CreateNewCurrencyParams) int {
	logHead := "[Models][CreateNewCurrency]"
	fmt.Printf("%s STARTS - params: %#v", logHead, params)

	id := -1
	insertionError := db.QueryRow(
		fmt.Sprintf("INSERT INTO currency (ticker) VALUES ($1) RETURNING id;"),
		params.Ticker,
	).Scan(&id)

	if insertionError != nil {
		fmt.Printf(
			"%s FAILED - Cannot insert new currency with ticker: '%s' in database. %s\n",
			logHead,
			params.Ticker,
			insertionError.Error(),
		)
		return id
	}

	fmt.Printf("%s DONE - currencyId: %d", logHead, id)
	return id
}
