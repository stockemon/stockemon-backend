package utils

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

func GeneratePasswordHash(password []byte) string {
	hash, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)

	if err != nil {
		fmt.Printf(
			"[Utils][GeneratePasswordHash] FAILED - Cannot create password hash: %s\n",
			err.Error(),
		)
		return ""
	}

	return string(hash)
}

func VerifyPassword(password []byte, hash []byte) bool {
	fmt.Printf("[Utils][VerifyPassword] STARTS - password: '%s', hash: '%s'\n", password, hash)

	err := bcrypt.CompareHashAndPassword(hash, password)

	if err != nil {
		return false
	}

	return true
}
