package utils

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

type Claim struct {
	jwt.MapClaims
	Uid    string `json:"uid"`
	Expiry int64  `json:"exp"`
}

func GetTokenAge() int {
	tokenExpiryInHour, err := strconv.Atoi(os.Getenv("TOKEN_EXPIRY_IN_HOUR"))

	if err != nil {
		// Default to be 24 hours expiry if no value is defined in .env
		tokenExpiryInHour = 24
	}

	return int((time.Hour * time.Duration(tokenExpiryInHour)).Seconds())
}

func ExtractTokenFromRequest(c *gin.Context) string {
	bearerToken := c.Request.Header.Get("Authorization")
	token := strings.ReplaceAll(bearerToken, "Bearer ", "")

	fmt.Printf("[Utils][ExtractTokenFromRequest] token: '%s'\n", token)

	return token
}

func GenerateToken(uid int) (string, error) {
	tokenExpiryInHour, err := strconv.Atoi(os.Getenv("TOKEN_EXPIRY_IN_HOUR"))

	if err != nil {
		// Default to be 24 hours expiry if no value is defined in .env
		tokenExpiryInHour = 24
	}

	claims := jwt.MapClaims{}
	claims["uid"] = strconv.Itoa(uid)
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(tokenExpiryInHour)).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	signedToken, err := token.SignedString([]byte(os.Getenv("TOKEN_SECRET")))

	if err != nil {
		return "", fmt.Errorf("Token generation failed.")
	}

	return signedToken, nil
}

func VerifyToken(token string) (bool, int) {
	logHead := "[Utils][VerifyToken]"

	t, err := jwt.ParseWithClaims(token, &Claim{}, func(tk *jwt.Token) (interface{}, error) {
		if _, isValidSigningMethod := tk.Method.(*jwt.SigningMethodHMAC); !isValidSigningMethod {
			return nil, fmt.Errorf("%s Unexpected signing method: %v", logHead, tk.Header["alg"])
		}

		return []byte(os.Getenv("TOKEN_SECRET")), nil
	})

	if err != nil || !t.Valid {
		fmt.Printf("%s Token invalid: %s", logHead, err.Error())
		return false, -1
	}

	claim := t.Claims.(*Claim)
	fmt.Printf("%s claim: %#v\n", logHead, claim)

	currentTime := time.Now().Unix()
	fmt.Printf("%s Time now: %d, Token Expiry: %d\n", logHead, currentTime, claim.Expiry)
	if claim.Expiry <= currentTime {
		return false, -1
	}

	uid, err := strconv.Atoi(claim.Uid)

	if err != nil {
		fmt.Printf("%s Invalid user id. %s\n", logHead, err.Error())
		return false, -1
	}

	fmt.Printf("%s Found user id: '%d'\n", logHead, uid)
	return true, uid
}
