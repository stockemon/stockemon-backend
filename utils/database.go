package utils

import (
	"database/sql"
	"fmt"
	"os"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

func ConnectDatabase(dbUrl string) (db *sql.DB, err error) {
	logHead := "[Utils][ConnectDatabase]"
	db, paramsError := sql.Open("postgres", dbUrl)

	fmt.Printf("%s Establishing connection to '%s'\n", logHead, dbUrl)

	if paramsError != nil {
		return nil, fmt.Errorf("%s %s", logHead, paramsError.Error())
	}

	if error := db.Ping(); error != nil {
		return nil, fmt.Errorf("%s Failed to establish database connection: %s", logHead, error.Error())
	}

	fmt.Printf("%s Database connection established\n", logHead)

	return db, nil
}

func MigrateDatabase(db *sql.DB) {
	logHead := "[Utils][MigrateDatabase]"
	driver, initDriverError := postgres.WithInstance(db, &postgres.Config{})

	if initDriverError != nil {
		fmt.Printf(
			"%s FAILED - Cannot initialize driver: %s\n",
			logHead,
			initDriverError.Error())
	} else {
		databaseName := os.Getenv("DATABASE_NAME")
		mdbi, prepDbInstanceError := migrate.NewWithDatabaseInstance(
			"file://migrations",
			databaseName,
			driver,
		)

		if prepDbInstanceError != nil {
			fmt.Printf(
				"%s FAILED - Cannot prepare migration database instance: %s\n",
				logHead,
				prepDbInstanceError.Error(),
			)
		} else {
			migrationError := mdbi.Up()

			if migrationError != nil {
				fmt.Printf(
					"%s FAILED - Cannot perform database migration: %s\n",
					logHead,
					migrationError.Error(),
				)
			} else {
				fmt.Printf("%s DONE\n", logHead)
			}
		}
	}
}
