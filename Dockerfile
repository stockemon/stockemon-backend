FROM golang:1.18-alpine AS builder

ARG GIT_TAG \
    GIT_COMMIT_MESSAGE \
    GIT_COMMIT_HASH \
    GITLAB_USER_NAME \
    IMAGE_BUILD_TIME \
    CI_PROJECT_URL \
    CI_PROJECT_NAME

LABEL go_version=1.18 \
      tag=$GIT_TAG \
      commit_message=$GIT_COMMIT_MESSAGE \
      commit_hash=$GIT_COMMIT_HASH \
      image_build_time=$IMAGE_BUILD_TIME \
      author=$GITLAB_USER_NAME \
      ci_project_url=$CI_PROJECT_URL \
      ci_project_name=$CI_PROJECT_NAME

WORKDIR /app
COPY go.mod go.mod
COPY go.sum go.sum
COPY main.go main.go
COPY handlers handlers
COPY middlewares middlewares
COPY migrations migrations
COPY models models
COPY utils utils
RUN CGO_ENABLED=0 go build -o stockemon-backend .

FROM golang:1.18-alpine

ARG DOMAIN \
    API_PORT \
    ALLOW_ORIGINS \
    POSTGRES_CONNECTION_URL \
    DATABASE_NAME \
    TOKEN_EXPIRY_IN_HOUR \
    TOKEN_SECRET

WORKDIR /app
COPY --from=builder /app/stockemon-backend .
RUN touch .env
RUN printf "DOMAIN=$DOMAIN \n\
API_PORT=$API_PORT \n\
ALLOW_ORIGINS=$ALLOW_ORIGINS \n\n\
POSTGRES_CONNECTION_URL=$POSTGRES_CONNECTION_URL \n\
DATABASE_NAME=$DATABASE_NAME \n\n\
TOKEN_EXPIRY_IN_HOUR=$TOKEN_EXPIRY_IN_HOUR \n\
TOKEN_SECRET=$TOKEN_SECRET" >> .env

EXPOSE $API_PORT

CMD ["/app/stockemon-backend"]
