# stockemon-backend

User authentication and authorization, stock portfolio management CRUD service for Stockemon application.

Languages and Frameworks

- [Go](https://go.dev) - Build fast, reliable, and efficient software at scale

## How To Run the Program

```bash
# Through cloning the repository
go run main.go
# Through docker image
docker pull registry.gitlab.com/stockemon/stockemon-backend:latest
docker run -d --name stockemon-backend -p 3000:3000 --platform linux/amd64 registry.gitlab.com/stockemon/stockemon-backend
# Enter container to view content
docker run -it --platform linux/amd64 registry.gitlab.com/stockemon/stockemon-backend sh
```

## Local Environment Setup

### Environment Variables

```bash
# .env
DOMAIN=localhost
API_PORT=3000
ALLOW_ORIGINS=http://localhost:8000

POSTGRES_CONNECTION_URL=postgres://root:root@0.0.0.0:5432/stockemon?sslmode=disable
DATABASE_NAME=stockemon

TOKEN_EXPIRY_IN_HOUR=1
TOKEN_SECRET=secret
```

### Golang Environment Variable Setup (ZSH)

```bash
export GOPATH=$HOME/go
export GOROOT=/usr/local/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOPATH
export PATH=$PATH:$GOROOT/bin
export PATH=$PATH:$GOPATH/bin
```

### Postgresql Docker Setup

```bash
# Setup container running postgres database
docker pull postgres:14.1

docker run -d --name postgres -e POSTGRES_USERNAME=root -e POSTGRES_PASSWORD=root -v ${HOME}/<path>/:/var/lib/postgresql/data -p 5432:5432 postgres:14.1

docker exec -it postgres bash

# Setup superuser root with password root
root@e0406f495e62:/ su - postgres

postgres@e0406f495e62:~$ createuser --interactive --pwprompt
```

### Migration Setup

```bash
go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

vim .zshrc
export PATH=$PATH:$GOPATH/bin

# Create new migration file
migrate create -ext sql -dir ./migrations -seq <migration_file_name>
```

### Database Schema

```bash
stockemon=# \d client
                                        Table "public.client"
    Column    |           Type           | Collation | Nullable |              Default               
--------------+--------------------------+-----------+----------+------------------------------------
 id           | integer                  |           | not null | nextval('client_id_seq'::regclass)
 username     | character varying(20)    |           | not null | 
 display_name | character varying(40)    |           |          | 
 password     | text                     |           | not null | 
 created_at   | timestamp with time zone |           |          | CURRENT_TIMESTAMP
 updated_at   | timestamp with time zone |           |          | CURRENT_TIMESTAMP
Indexes:
    "client_pkey" PRIMARY KEY, btree (id)
    "client_username_key" UNIQUE CONSTRAINT, btree (username)

stockemon=# \d portfolio
                                          Table "public.portfolio"
   Column   |           Type           | Collation | Nullable |                   Default                   
------------+--------------------------+-----------+----------+---------------------------------------------
 id         | integer                  |           | not null | nextval('portfolio_id_seq'::regclass)
 name       | character varying(50)    |           | not null | 
 currency   | integer                  |           | not null | nextval('portfolio_currency_seq'::regclass)
 created_at | timestamp with time zone |           |          | CURRENT_TIMESTAMP
 updated_at | timestamp with time zone |           |          | CURRENT_TIMESTAMP
Indexes:
    "portfolio_pkey" PRIMARY KEY, btree (id)
Foreign-key constraints:
    "portfolio_currency_fkey" FOREIGN KEY (currency) REFERENCES currency(id)
Referenced by:
    TABLE "cash_transaction" CONSTRAINT "cash_transaction_portfolio_id_fkey" FOREIGN KEY (portfolio_id) REFERENCES portfolio(id)
    TABLE "stock_holding" CONSTRAINT "stock_holding_portfolio_id_fkey" FOREIGN KEY (portfolio_id) REFERENCES portfolio(id)

stockemon=# \d currency
                                       Table "public.currency"
   Column   |           Type           | Collation | Nullable |               Default                
------------+--------------------------+-----------+----------+--------------------------------------
 id         | integer                  |           | not null | nextval('currency_id_seq'::regclass)
 ticker     | character varying(3)     |           | not null | 
 created_at | timestamp with time zone |           |          | CURRENT_TIMESTAMP
 updated_at | timestamp with time zone |           |          | CURRENT_TIMESTAMP
Indexes:
    "currency_pkey" PRIMARY KEY, btree (id)
    "currency_ticker_key" UNIQUE CONSTRAINT, btree (ticker)
Referenced by:
    TABLE "cash_transaction" CONSTRAINT "cash_transaction_currency_fkey" FOREIGN KEY (currency) REFERENCES currency(id)
    TABLE "portfolio" CONSTRAINT "portfolio_currency_fkey" FOREIGN KEY (currency) REFERENCES currency(id)
    TABLE "stock" CONSTRAINT "stock_currency_fkey" FOREIGN KEY (currency) REFERENCES currency(id)

stockemon=# \d client_portfolio
                                             Table "public.client_portfolio"
    Column    |           Type           | Collation | Nullable |                        Default                         
--------------+--------------------------+-----------+----------+--------------------------------------------------------
 client_id    | integer                  |           | not null | nextval('client_portfolio_client_id_seq'::regclass)
 portfolio_id | integer                  |           | not null | nextval('client_portfolio_portfolio_id_seq'::regclass)
 created_at   | timestamp with time zone |           |          | CURRENT_TIMESTAMP
 updated_at   | timestamp with time zone |           |          | CURRENT_TIMESTAMP
Foreign-key constraints:
    "client_portfolio_client_id_fkey" FOREIGN KEY (client_id) REFERENCES client(id)
    "client_portfolio_portfolio_id_fkey" FOREIGN KEY (portfolio_id) REFERENCES portfolio(id)

stockemon=# \d stock
                                            Table "public.stock"
     Column     |           Type           | Collation | Nullable |                 Default                 
----------------+--------------------------+-----------+----------+-----------------------------------------
 id             | integer                  |           | not null | nextval('stock_id_seq'::regclass)
 ticker         | character varying(10)    |           | not null | 
 name           | text                     |           |          | 
 last_end_price | text                     |           |          | 
 price          | text                     |           |          | 
 currency       | integer                  |           | not null | nextval('stock_currency_seq'::regclass)
 created_at     | timestamp with time zone |           |          | CURRENT_TIMESTAMP
 updated_at     | timestamp with time zone |           |          | CURRENT_TIMESTAMP
Indexes:
    "stock_pkey" PRIMARY KEY, btree (id)
    "stock_ticker_key" UNIQUE CONSTRAINT, btree (ticker)
Foreign-key constraints:
    "stock_currency_fkey" FOREIGN KEY (currency) REFERENCES currency(id)
Referenced by:
    TABLE "stock_holding" CONSTRAINT "stock_holding_stock_id_fkey" FOREIGN KEY (stock_id) REFERENCES stock(id)

stockemon=# \d stock_holding
                                             Table "public.stock_holding"
    Column    |           Type           | Collation | Nullable |                       Default                       
--------------+--------------------------+-----------+----------+-----------------------------------------------------
 id           | integer                  |           | not null | nextval('stock_holding_id_seq'::regclass)
 portfolio_id | integer                  |           | not null | nextval('stock_holding_portfolio_id_seq'::regclass)
 stock_id     | integer                  |           | not null | nextval('stock_holding_stock_id_seq'::regclass)
 cost         | text                     |           | not null | 
 shares       | text                     |           | not null | 
 fee          | text                     |           | not null | 
 note         | text                     |           | not null | 
 created_at   | timestamp with time zone |           |          | CURRENT_TIMESTAMP
 updated_at   | timestamp with time zone |           |          | CURRENT_TIMESTAMP
Indexes:
    "stock_holding_pkey" PRIMARY KEY, btree (id)
Foreign-key constraints:
    "stock_holding_portfolio_id_fkey" FOREIGN KEY (portfolio_id) REFERENCES portfolio(id)
    "stock_holding_stock_id_fkey" FOREIGN KEY (stock_id) REFERENCES stock(id)

stockemon=# \d cash_transaction
                                             Table "public.cash_transaction"
    Column    |           Type           | Collation | Nullable |                        Default                        
 
--------------+--------------------------+-----------+----------+-------------------------------------------------------
-
 id           | integer                  |           | not null | nextval('cash_transaction_id_seq'::regclass)
 portfolio_id | integer                  |           | not null | nextval('cash_transaction_portfolio_id_seq'::regclass)
 amount       | text                     |           | not null | 
 currency     | integer                  |           | not null | nextval('cash_transaction_currency_seq'::regclass)
 note         | text                     |           | not null | 
 created_at   | timestamp with time zone |           |          | CURRENT_TIMESTAMP
 updated_at   | timestamp with time zone |           |          | CURRENT_TIMESTAMP
Indexes:
    "cash_transaction_pkey" PRIMARY KEY, btree (id)
Foreign-key constraints:
    "cash_transaction_currency_fkey" FOREIGN KEY (currency) REFERENCES currency(id)
    "cash_transaction_portfolio_id_fkey" FOREIGN KEY (portfolio_id) REFERENCES portfolio(id)
```
