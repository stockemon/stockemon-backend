CREATE TABLE IF NOT EXISTS cash_transaction (
  id                SERIAL                            PRIMARY KEY,
  portfolio_id      SERIAL                            REFERENCES portfolio (id),
  amount            TEXT                              NOT NULL,
  currency          SERIAL                            REFERENCES currency (id),
  note              TEXT                              NOT NULL,
  created_at        TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP,
  updated_at        TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP
);
