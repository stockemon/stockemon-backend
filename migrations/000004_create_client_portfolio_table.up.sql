CREATE TABLE IF NOT EXISTS client_portfolio (
  client_id     SERIAL                            REFERENCES client (id),
  portfolio_id  SERIAL                            REFERENCES portfolio (id),
  created_at    TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP,
  updated_at    TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP
);
