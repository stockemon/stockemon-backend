CREATE TABLE IF NOT EXISTS client (
  id            SERIAL                            PRIMARY KEY,
  username      VARCHAR(20)                       UNIQUE NOT NULL,
  display_name  VARCHAR(40),
  password      TEXT                              NOT NULL,
  created_at    TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP,
  updated_at    TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP
);
