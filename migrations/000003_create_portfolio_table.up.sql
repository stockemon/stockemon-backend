CREATE TABLE IF NOT EXISTS portfolio (
  id          SERIAL                            PRIMARY KEY,
  name        VARCHAR(50)                       NOT NULL,
  currency    SERIAL                            REFERENCES currency (id),
  created_at  TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP,
  updated_at  TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP
);
