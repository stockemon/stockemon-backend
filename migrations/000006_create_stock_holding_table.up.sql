CREATE TABLE IF NOT EXISTS stock_holding (
  id                SERIAL                            PRIMARY KEY,
  portfolio_id      SERIAL                            REFERENCES portfolio (id),
  stock_id          SERIAL                            REFERENCES stock (id),
  cost              TEXT                              NOT NULL,
  shares            TEXT                              NOT NULL,
  fee               TEXT                              NOT NULL,
  note              TEXT                              NOT NULL,
  created_at        TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP,
  updated_at        TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP
);
