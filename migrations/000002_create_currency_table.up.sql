CREATE TABLE IF NOT EXISTS currency (
  id          SERIAL                            PRIMARY KEY,
  ticker      VARCHAR(3)                        UNIQUE NOT NULL,
  created_at  TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP,
  updated_at  TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP
);
