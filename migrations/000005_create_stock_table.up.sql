CREATE TABLE IF NOT EXISTS stock (
  id                SERIAL                            PRIMARY KEY,
  ticker            VARCHAR(10)                       UNIQUE NOT NULL,
  name              TEXT,
  last_end_price    TEXT,
  price             TEXT,
  currency          SERIAL                            REFERENCES currency (id),
  created_at        TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP,
  updated_at        TIMESTAMP WITH TIME ZONE DEFAULT  CURRENT_TIMESTAMP
);
