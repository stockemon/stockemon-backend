package middlewares

import (
	"net/http"
	"stockemon-backend/handlers"
	"stockemon-backend/utils"

	"github.com/gin-gonic/gin"
)

func AuthorizationMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Bearer authorization implementation
		// token := utils.ExtractTokenFromRequest(c)
		token, getCookieError := c.Request.Cookie("token")

		if getCookieError != nil {
			var bodyToken handlers.TokenObject
			c.ShouldBindJSON(&bodyToken)

			isTokenValid, uid := utils.VerifyToken(bodyToken.Token)

			if !isTokenValid {
				c.JSON(http.StatusUnauthorized, gin.H{"success": false, "error": "Unauthorized access."})
				c.Abort()
				return
			}

			c.Set("token", uid)
			c.Next()
			return
		}

		isTokenValid, uid := utils.VerifyToken(token.Value)

		if !isTokenValid {
			c.JSON(http.StatusUnauthorized, gin.H{"success": false, "error": "Unauthorized access."})
			c.Abort()
			return
		}

		c.Set("token", uid)
		c.Next()
		return
	}
}
