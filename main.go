package main

import (
	"fmt"
	"os"
	"stockemon-backend/handlers"
	"stockemon-backend/middlewares"
	"stockemon-backend/utils"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload"
)

func main() {
	// Establish database connection
	dbUrl := os.Getenv("POSTGRES_CONNECTION_URL")
	db, err := utils.ConnectDatabase(dbUrl)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	// Database migration
	utils.MigrateDatabase(db)

	// API service initialization
	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{os.Getenv("ALLOW_ORIGINS")},
		AllowHeaders:     []string{"Content-Type"},
		AllowCredentials: true,
	}))

	v1 := router.Group("/api/v1")

	auth := v1.Group("/auth")
	{
		AuthHandler := new(handlers.AuthHandler)
		AuthHandler.Db = db

		auth.POST("/login", AuthHandler.Login)
		auth.POST("/signup", AuthHandler.Signup)
		auth.POST("/refresh", middlewares.AuthorizationMiddleware(), AuthHandler.Refresh)
		auth.POST("/verify", middlewares.AuthorizationMiddleware(), AuthHandler.Verify)
	}

	// portfolio := v1.Group("/portfolio")
	// {

	// }

	apiPort := os.Getenv("API_PORT")
	router.Run(":" + apiPort)
}
