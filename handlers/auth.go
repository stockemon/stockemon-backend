package handlers

import (
	"database/sql"
	"net/http"
	"os"
	"stockemon-backend/models"
	"stockemon-backend/utils"

	"github.com/gin-gonic/gin"
)

var ClientModel = new(models.ClientModel)

type AuthHandler struct {
	Db *sql.DB
}

type LoginRequestBody struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type TokenObject struct {
	Token string `json:"token"`
}

func (u AuthHandler) Login(c *gin.Context) {
	var data LoginRequestBody

	if c.ShouldBindJSON(&data) != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": "Missing required fields."})
		return
	}

	foundClient, client := ClientModel.GetClientByName(u.Db, data.Username)

	if !foundClient {
		c.JSON(http.StatusNotFound, gin.H{"success": false, "error": "User invalid."})
		return
	}

	hash := []byte(data.Password)
	dbHash := []byte(client.Password)
	isPasswordCorrect := utils.VerifyPassword(hash, dbHash)

	if !isPasswordCorrect {
		c.JSON(http.StatusForbidden, gin.H{"success": false, "error": "Password incorrect."})
		return
	}

	token, err := utils.GenerateToken(client.Id)

	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"success": false, "error": err.Error()})
		return
	}

	c.SetCookie("token", token, utils.GetTokenAge(), "/", os.Getenv("DOMAIN"), false, true)
	c.JSON(http.StatusOK, gin.H{"success": true, "data": TokenObject{Token: token}})
}

func (u AuthHandler) Signup(c *gin.Context) {
	var data models.CreateNewClientParams

	if c.ShouldBindJSON(&data) != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": "Missing required fields."})
		return
	}

	if len(data.Username) > 20 {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": "Username exceeds maximum length of 20 characters."})
		return
	}

	if len(data.DisplayName) > 50 {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": "Display name exceeds maximum length of 50 characters."})
		return
	}

	foundClient, _ := ClientModel.GetClientByName(u.Db, data.Username)

	if foundClient {
		c.JSON(http.StatusForbidden, gin.H{"success": false, "error": "Username already in use."})
		return
	}

	uid := ClientModel.CreateNewClient(u.Db, data)

	if uid < 0 {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": "Unexpected error."})
		return
	}

	token, err := utils.GenerateToken(uid)

	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"success": false, "error": err.Error()})
		return
	}

	c.SetCookie("token", token, utils.GetTokenAge(), "/", os.Getenv("DOMAIN"), false, true)
	c.JSON(http.StatusCreated, gin.H{"success": true, "data": TokenObject{Token: token}})
}

func (u AuthHandler) Refresh(c *gin.Context) {
	c.JSON(http.StatusAccepted, gin.H{"success": true})
}

func (u AuthHandler) Verify(c *gin.Context) {
	c.JSON(http.StatusAccepted, gin.H{"success": true})
}
